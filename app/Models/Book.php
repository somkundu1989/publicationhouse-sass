<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Book
 * @package App\Models
 * @version October 19, 2020, 7:36 pm UTC
 *
 * @property \App\Models\Author $author
 * @property \App\Models\Publicationhouse $publicationhouse
 * @property string $title
 * @property integer $author_id
 * @property integer $publicationhouse_id
 * @property number $price
 * @property boolean $no_of_pages
 * @property boolean $status
 */
class Book extends Model
{

    public $table = 'books';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'title',
        'author_id',
        'publicationhouse_id',
        'price',
        'no_of_pages',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'author_id' => 'integer',
        'publicationhouse_id' => 'integer',
        'price' => 'float',
        'no_of_pages' => 'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
        'author_id' => 'nullable|integer',
        'publicationhouse_id' => 'nullable|integer',
        'price' => 'nullable|numeric',
        'no_of_pages' => 'nullable|integer',
        'status' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function author()
    {
        return $this->belongsTo(\App\Models\Author::class, 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function publicationhouse()
    {
        return $this->belongsTo(\App\Models\Publicationhouse::class, 'publicationhouse_id');
    }
}
