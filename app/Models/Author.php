<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Author
 * @package App\Models
 * @version October 19, 2020, 7:37 pm UTC
 *
 * @property \App\Models\Publicationhouse $publicationhouse
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $books
 * @property integer $user_id
 * @property string $taken_name
 * @property integer $publicationhouse_id
 */
class Author extends Model
{

    public $table = 'authors';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'user_id',
        'taken_name',
        'publicationhouse_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'taken_name' => 'string',
        'publicationhouse_id' => 'integer'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function publicationhouse()
    {
        return $this->belongsTo(\App\Models\Publicationhouse::class, 'publicationhouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function books()
    {
        return $this->hasMany(\App\Models\Book::class, 'author_id');
    }
}
