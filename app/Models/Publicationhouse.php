<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Publicationhouse
 * @package App\Models
 * @version October 19, 2020, 7:07 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $authors
 * @property \Illuminate\Database\Eloquent\Collection $books
 * @property string $title
 * @property string $established_at
 * @property string $owner_name
 */
class Publicationhouse extends Model
{

    public $table = 'publicationhouses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'title',
        'established_at',
        'owner_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'established_at' => 'date',
        'owner_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string|max:255',
        'established_at' => 'nullable',
        'owner_name' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function authors()
    {
        return $this->hasMany(\App\Models\Author::class, 'publicationhouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function books()
    {
        return $this->hasMany(\App\Models\Book::class, 'publicationhouse_id');
    }
}
