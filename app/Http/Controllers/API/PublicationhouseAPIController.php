<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePublicationhouseAPIRequest;
use App\Http\Requests\API\UpdatePublicationhouseAPIRequest;
use App\Models\Publicationhouse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PublicationhouseController
 * @package App\Http\Controllers\API
 */

class PublicationhouseAPIController extends AppBaseController
{
    /**
     * Display a listing of the Publicationhouse.
     * GET|HEAD /publicationhouses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Publicationhouse::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $publicationhouses = $query->get();

        return $this->sendResponse($publicationhouses->toArray(), 'Publicationhouses retrieved successfully');
    }

    /**
     * Store a newly created Publicationhouse in storage.
     * POST /publicationhouses
     *
     * @param CreatePublicationhouseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePublicationhouseAPIRequest $request)
    {
        $input = $request->all();

        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::create($input);

        return $this->sendResponse($publicationhouse->toArray(), 'Publicationhouse saved successfully');
    }

    /**
     * Display the specified Publicationhouse.
     * GET|HEAD /publicationhouses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            return $this->sendError('Publicationhouse not found');
        }

        return $this->sendResponse($publicationhouse->toArray(), 'Publicationhouse retrieved successfully');
    }

    /**
     * Update the specified Publicationhouse in storage.
     * PUT/PATCH /publicationhouses/{id}
     *
     * @param int $id
     * @param UpdatePublicationhouseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePublicationhouseAPIRequest $request)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            return $this->sendError('Publicationhouse not found');
        }

        $publicationhouse->fill($request->all());
        $publicationhouse->save();

        return $this->sendResponse($publicationhouse->toArray(), 'Publicationhouse updated successfully');
    }

    /**
     * Remove the specified Publicationhouse from storage.
     * DELETE /publicationhouses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            return $this->sendError('Publicationhouse not found');
        }

        $publicationhouse->delete();

        return $this->sendSuccess('Publicationhouse deleted successfully');
    }
}
