<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAuthorAPIRequest;
use App\Http\Requests\API\UpdateAuthorAPIRequest;
use App\Models\Author;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Hash;

/**
 * Class AuthorController
 * @package App\Http\Controllers\API
 */

class AuthorAPIController extends AppBaseController
{
    /**
     * Display a listing of the Author.
     * GET|HEAD /authors
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Author::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $authors = $query->with('user')->get();

        return $this->sendResponse($authors->toArray(), 'Authors retrieved successfully');
    }

    /**
     * Store a newly created Author in storage.
     * POST /authors
     *
     * @param CreateAuthorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAuthorAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
        /** @var Author $author */
        $author = Author::create([
            'user_id' => $user->id,
            'taken_name' => $input['taken_name'],
            'publicationhouse_id' => $input['publicationhouse_id']
        ]);

        return $this->sendResponse($author->toArray(), 'Author saved successfully');
    }

    /**
     * Display the specified Author.
     * GET|HEAD /authors/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Author $author */
        $author = Author::with('user')->find($id);

        if (empty($author)) {
            return $this->sendError('Author not found');
        }

        return $this->sendResponse($author->toArray(), 'Author retrieved successfully');
    }

    /**
     * Update the specified Author in storage.
     * PUT/PATCH /authors/{id}
     *
     * @param int $id
     * @param UpdateAuthorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAuthorAPIRequest $request)
    {
        $input = $request->all();

        /** @var Author $author */
        $author = Author::find($id);

        if (empty($author)) {
            return $this->sendError('Author not found');
        }

        $author->fill([
            'taken_name' => $input['taken_name'],
            'publicationhouse_id' => $input['publicationhouse_id']
        ]);
        $author->save();

        /** @var User $user */
        $user = User::find($author->user_id);
        $userFields = [
            'name' => $input['name'],
            'email' => $input['email'],
        ];
        if( $input['password'] != null && $input['password_confirmation'] != null ){
            $userFields['password'] = Hash::make($input['password']);
        }
        // dd($userFields);
        $user->fill($userFields)->save();


        return $this->sendResponse($author->toArray(), 'Author updated successfully');
    }

    /**
     * Remove the specified Author from storage.
     * DELETE /authors/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Author $author */
        $author = Author::find($id);

        if (empty($author)) {
            return $this->sendError('Author not found');
        }

        $author->delete();

        return $this->sendSuccess('Author deleted successfully');
    }
}
