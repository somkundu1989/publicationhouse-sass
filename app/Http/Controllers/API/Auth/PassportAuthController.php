<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class PassportAuthController extends AppBaseController
{
 
    /**
     * Login Req
     */
    public function login(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:4',
        ]);
        if ($validator->fails())
        {
            return $this->sendError('Wrong email or password provided');
        }
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken($request->email)->accessToken;

            return $this->sendResponse(['token' => $token], 'Successfully checked in');
        } 
        return $this->sendError('Unauthorised access');
    }
}
