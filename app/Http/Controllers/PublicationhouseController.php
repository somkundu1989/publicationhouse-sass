<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePublicationhouseRequest;
use App\Http\Requests\UpdatePublicationhouseRequest;
// use App\Http\Controllers\AppBaseController;
use App\Models\Publicationhouse;
use Illuminate\Http\Request;
use Flash;
use Response;

class PublicationhouseController extends AppBaseController
{
    /**
     * Display a listing of the Publicationhouse.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Publicationhouse $publicationhouses */
        $publicationhouses = Publicationhouse::all();

        return view('publicationhouses.index')
            ->with('publicationhouses', $publicationhouses);
    }

    /**
     * Show the form for creating a new Publicationhouse.
     *
     * @return Response
     */
    public function create()
    {
        return view('publicationhouses.create');
    }

    /**
     * Store a newly created Publicationhouse in storage.
     *
     * @param CreatePublicationhouseRequest $request
     *
     * @return Response
     */
    public function store(CreatePublicationhouseRequest $request)
    {
        $input = $request->all();

        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::create($input);

        Flash::success('Publicationhouse saved successfully.');

        return redirect(route('publicationhouses.index'));
    }

    /**
     * Display the specified Publicationhouse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            Flash::error('Publicationhouse not found');

            return redirect(route('publicationhouses.index'));
        }

        return view('publicationhouses.show')->with('publicationhouse', $publicationhouse);
    }

    /**
     * Show the form for editing the specified Publicationhouse.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            Flash::error('Publicationhouse not found');

            return redirect(route('publicationhouses.index'));
        }

        return view('publicationhouses.edit')->with('publicationhouse', $publicationhouse);
    }

    /**
     * Update the specified Publicationhouse in storage.
     *
     * @param int $id
     * @param UpdatePublicationhouseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePublicationhouseRequest $request)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            Flash::error('Publicationhouse not found');

            return redirect(route('publicationhouses.index'));
        }

        $publicationhouse->fill($request->all());
        $publicationhouse->save();

        Flash::success('Publicationhouse updated successfully.');

        return redirect(route('publicationhouses.index'));
    }

    /**
     * Remove the specified Publicationhouse from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Publicationhouse $publicationhouse */
        $publicationhouse = Publicationhouse::find($id);

        if (empty($publicationhouse)) {
            Flash::error('Publicationhouse not found');

            return redirect(route('publicationhouses.index'));
        }

        $publicationhouse->delete();

        Flash::success('Publicationhouse deleted successfully.');

        return redirect(route('publicationhouses.index'));
    }
}
