<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Author;
use App\Models\User;
use Illuminate\Http\Request;
use Flash;
use Response;
use Hash;

class AuthorController extends AppBaseController
{
    /**
     * Display a listing of the Author.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Author $authors */
        $authors = Author::all();

        return view('authors.index')
            ->with('authors', $authors);
    }

    /**
     * Show the form for creating a new Author.
     *
     * @return Response
     */
    public function create()
    {
        return view('authors.create');
    }

    /**
     * Store a newly created Author in storage.
     *
     * @param CreateAuthorRequest $request
     *
     * @return Response
     */
    public function store(CreateAuthorRequest $request)
    {
        $input = $request->all();
        // dd($input);
        /** @var User $user */
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
        /** @var Author $author */
        $author = Author::create([
            'user_id' => $user->id,
            'taken_name' => $input['taken_name'],
            'publicationhouse_id' => $input['publicationhouse_id']
        ]);

        Flash::success('Author saved successfully.');

        return redirect(route('authors.index'));
    }

    /**
     * Display the specified Author.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Author $author */
        $author = Author::find($id);

        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }

        return view('authors.show')->with('author', $author);
    }

    /**
     * Show the form for editing the specified Author.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Author $author */
        $author = Author::find($id);

        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }        

        return view('authors.edit')->with('author', $author);
    }

    /**
     * Update the specified Author in storage.
     *
     * @param int $id
     * @param UpdateAuthorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAuthorRequest $request)
    {
        $input = $request->all();
        
        /** @var Author $author */
        $author = Author::find($id);
        
        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }

        $author->fill([
            'taken_name' => $input['taken_name'],
            'publicationhouse_id' => $input['publicationhouse_id']
        ]);
        $author->save();

        /** @var User $user */
        $user = User::find($author->user_id);
        $userFields = [
            'name' => $input['name'],
            'email' => $input['email'],
        ];
        if( $input['password'] != null && $input['password_confirmation'] != null ){
            $userFields['password'] = Hash::make($input['password']);
        }
        // dd($userFields);
        $user->fill($userFields)->save();


        Flash::success('Author updated successfully.');

        return redirect(route('authors.index'));
    }

    /**
     * Remove the specified Author from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Author $author */
        $author = Author::find($id);

        if (empty($author)) {
            Flash::error('Author not found');

            return redirect(route('authors.index'));
        }

        $author->delete();

        Flash::success('Author deleted successfully.');

        return redirect(route('authors.index'));
    }
}
