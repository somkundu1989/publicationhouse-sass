<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
// use App\Models\Author;
use Illuminate\Http\Request;

class UpdateAuthorRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'taken_name' => 'nullable|string|max:255',
            'publicationhouse_id' => 'nullable|integer',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email, '.$request->user_id],
            //'password' => ['required', 'string', 'min:4', 'confirmed'],
        ];
        if($request->password != null || $request->password_confirmation != null){
            $rules['password'] =  ['required', 'string', 'min:4', 'confirmed'];
        }
        
        return $rules;
    }
}
