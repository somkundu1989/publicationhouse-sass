<?php

namespace App\Http\Requests\API;

// use App\Models\Author;
use InfyOm\Generator\Request\APIRequest;

class CreateAuthorAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'taken_name' => 'nullable|string|max:255',
            'publicationhouse_id' => 'nullable|integer',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
        ];
    }
}
