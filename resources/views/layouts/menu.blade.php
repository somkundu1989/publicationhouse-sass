@if(Auth::user()->is_admin == 1)

<li class="{{ Request::is('publicationhouses*') ? 'active' : '' }}">
    <a href="{{ route('publicationhouses.index') }}"><i class="fa fa-edit"></i><span>Publicationhouses</span></a>
</li>

<li class="{{ Request::is('books*') ? 'active' : '' }}">
    <a href="{{ route('books.index') }}"><i class="fa fa-edit"></i><span>Books</span></a>
</li>

<li class="{{ Request::is('authors*') ? 'active' : '' }}">
    <a href="{{ route('authors.index') }}"><i class="fa fa-edit"></i><span>Authors</span></a>
</li>

@else

<li class="{{ Request::is('books*') ? 'active' : '' }}">
    <a href="{{ route('books.index') }}"><i class="fa fa-edit"></i><span>Books</span></a>
</li>

@endif
