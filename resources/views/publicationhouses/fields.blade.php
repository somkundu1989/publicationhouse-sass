<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Established At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('established_at', 'Established At:') !!}
    {!! Form::text('established_at', null, ['class' => 'form-control','id'=>'established_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#established_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Owner Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_name', 'Owner Name:') !!}
    {!! Form::text('owner_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('publicationhouses.index') }}" class="btn btn-default">Cancel</a>
</div>
