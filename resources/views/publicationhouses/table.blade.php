<div class="table-responsive">
    <table class="table" id="publicationhouses-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Established At</th>
        <th>Owner Name</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($publicationhouses as $publicationhouse)
            <tr>
                <td>{{ $publicationhouse->title }}</td>
            <td>{{ $publicationhouse->established_at }}</td>
            <td>{{ $publicationhouse->owner_name }}</td>
                <td>
                    {!! Form::open(['route' => ['publicationhouses.destroy', $publicationhouse->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('publicationhouses.show', [$publicationhouse->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('publicationhouses.edit', [$publicationhouse->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
