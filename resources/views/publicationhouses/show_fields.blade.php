<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $publicationhouse->title }}</p>
</div>

<!-- Established At Field -->
<div class="form-group">
    {!! Form::label('established_at', 'Established At:') !!}
    <p>{{ $publicationhouse->established_at }}</p>
</div>

<!-- Owner Name Field -->
<div class="form-group">
    {!! Form::label('owner_name', 'Owner Name:') !!}
    <p>{{ $publicationhouse->owner_name }}</p>
</div>

