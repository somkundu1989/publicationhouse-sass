<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $book->title }}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author', 'Author:') !!}
    <p>{{ $book->author->user->name }}({{ $book->author->taken_name }})</p>
</div>

<!-- Publicationhouse Id Field -->
<div class="form-group">
    {!! Form::label('publicationhouse', 'Publicationhouse:') !!}
    <p>{{ $book->publicationhouse->title }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $book->price }}</p>
</div>

<!-- No Of Pages Field -->
<div class="form-group">
    {!! Form::label('no_of_pages', 'No Of Pages:') !!}
    <p>{{ $book->no_of_pages }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $book->status }}</p>
</div>

