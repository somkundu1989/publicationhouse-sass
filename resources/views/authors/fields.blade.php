@if(!empty($author))
	<input type="hidden" name="user_id" value="{!! $author->user_id !!}">
@endif
<!-- Publicationhouse Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publicationhouse_id', 'Publicationhouse Id:') !!}
    {!! Form::number('publicationhouse_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Full Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Full Name:') !!}
    {!! Form::text('name', (!empty($author)?$author->user->name:null), ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>


<!-- Taken Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('taken_name', 'Taken Name:') !!}
    {!! Form::text('taken_name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', (!empty($author)?$author->user->email:null), ['class' => 'form-control','maxlength' => 255,'maxlength' => 255,'maxlength' => 255]) !!}
</div>


<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', '', ['class' => 'form-control','maxlength' => 15,'maxlength' => 15,'maxlength' => 15]) !!}
</div>

<!-- Password Confirmation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
    {!! Form::text('password_confirmation', '', ['class' => 'form-control','maxlength' => 15,'maxlength' => 15,'maxlength' => 15]) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('authors.index') }}" class="btn btn-default">Cancel</a>
</div>
