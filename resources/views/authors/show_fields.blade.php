
<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $author->user->name }}</p>
</div>

<!-- Taken Name Field -->
<div class="form-group">
    {!! Form::label('taken_name', 'Taken Name:') !!}
    <p>{{ $author->taken_name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $author->user->email }}</p>
</div>

<!-- Publicationhouse Id Field -->
<div class="form-group">
    {!! Form::label('publicationhouse', 'Publicationhouse:') !!}
    <p>{{ $author->publicationhouse->title }}</p>
</div>


