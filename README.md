






-----------------------------------------------------------------------------------
Project 1: Bookstore API
A bookstore API that CRUDs authors, books, and publications.
Objectives:
- Making requests to the API should only be possible through the use of an API key.
- There should be an API key management feature that enables CRUDing API keys.
- To access the API, users should be authenticated.
- Provide a clear and easy to use the documentation for the API
Requirements:
- Authors should be able to CRUD books.
- Authors should be able to add/remove books into/from publications.
- Authors should only have access to books that are associated to their account.
- The admin should be able to CRUD publications, authors, and books.
- Deploy top a staging(test) URL and provide logins
- Show all code on an open Git repository





route list for api's 


api/login => post => request{ email, password } => response{ token } 


api/authors => {bearer-token : token} => get => request{ token } => response{ list of authors }

api/authors => { bearer-token : token } => post => request{ name, email, taken_name, password } => response{ author created }

api/authors/{author_id} => { bearer-token : token } => delete => response{ author deleted }

api/authors/{author_id} => { bearer-token : token } => put => request{ name, email, taken_name, password } => response{ author updated }

api/authors/{author_id} => { bearer-token : token } => get => response{ author display }    


api/books => { bearer-token : token } => get => request{ token } => response{ list of authors }

api/books => { bearer-token : token } => post => request{ title, author_id, publicationhouse_id, price, no_of_pages, status } => response{ author created }

api/books/{book_id} => { bearer-token : token } => delete => response{ author deleted }

api/books/{book_id} => { bearer-token : token } => put => request{ title, author_id, publicationhouse_id, price, no_of_pages, status } => response{ author updated }

api/books/{book_id} => { bearer-token : token } => get => response{ author display }    


api/publicationhouses => { bearer-token : token } => get => request{ token } => response{ list of publicationhouses }

api/publicationhouses => { bearer-token : token } => post => request{ title, established_at, owner_name } => response{ publicationhouse created }

api/publicationhouses/{publicationhouse_id} => { bearer-token : token } => delete => response{ publicationhouse deleted }

api/publicationhouses/{publicationhouse_id} => { bearer-token : token } => put => request{ title, established_at, owner_name } => response{ publicationhouse updated }

api/publicationhouses/{publicationhouse_id} => { bearer-token : token } => get => response{ publicationhouse display }    






