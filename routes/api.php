<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'App\Http\Controllers\API\Auth\PassportAuthController@login');

Route::middleware('auth:api')->group(function () {
	Route::resource('publicationhouses', App\Http\Controllers\API\PublicationhouseAPIController::class);

	Route::resource('books', App\Http\Controllers\API\BookAPIController::class);

	Route::resource('authors', App\Http\Controllers\API\AuthorAPIController::class);

});